eval "$(zoxide init --cmd cd zsh)"
eval "$(starship init zsh)"
alias lg=lazygit
alias ls=exa
alias pcount='pacman -Q | wc -l'
alias yarn='yarn --use-yarnrc "$XDG_CONFIG_HOME/yarn/config"'

fpath=(/usr/share/zsh/site-functions/ $fpath)
HISTFILE=~/.config/zsh/.zsh_history
HISTSIZE=1000
SAVEHIST=1000
setopt SHARE_HISTORY
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

autoload compinit
compinit -i

fastfetch
