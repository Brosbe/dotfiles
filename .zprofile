#!/bin/sh

export EDITOR="nvim"
export BROWSER="firefox-nightly"
export TERMINAL="st"

export PATH="$PATH:$(find ~/.local/bin -type d | paste -sd ':' -)"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CURRENT_DESKTOP="dwl"

export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc":"$XDG_CONFIG_HOME/gtk-2.0/gtkrc.mine"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export TS3_CONFIG_DIR="$XDG_CONFIG_HOME/ts3client"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export GOPATH="$XDG_DATA_HOME/go"
export GOMODCACHE="$XDG_CACHE_HOME/go/mod"
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages/
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"
export OMNISHARPHOME="$XDG_CONFIG_HOME/omnisharp"
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export STACK_XDG=1

audio-reset
